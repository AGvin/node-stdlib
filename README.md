# Node.js StdLib


## HowTo use:

### Class (Inheritance)

```
#!javascript
'use strict';

var stdLib = require('node-stdlib'),

	Device = stdLib.Class.extend({
		type : null,
		vendor : null,

		init : function(vendor) {
			this.type = type;
		},
		getType : function() {
			return this.type;
		},
		getVendor  : function() {
			return this.vendor;
		},
		getDeviceInfo: function() {
			return 'Device type: ' this.getType() + ', vendor: ' + this.getVendor();
		},
	}),

	Phone = Device.extend({
		type : 'phone',
		os : 'android',

		getOs: function() {
			return this.os;
		},
		getDeviceInfo: function() {
			return this._super() + ', os: ' this.getOs();
		}
	}),

	myPhone = new Phone('my-vendor');

console.log(
	myPhone.getDeviceInfo()
);

```